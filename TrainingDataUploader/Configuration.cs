﻿using YamlDotNet.Serialization;
using System.Collections.Generic;

namespace TrainingDataUploader
{
    internal class Configuration
    {
        private static readonly IDeserializer Deserializer = new DeserializerBuilder().Build();

        public static Trainer Parse(string text)
        {
            return Deserializer.Deserialize<Trainer>(text);
        }

        public class Trainer
        {
            public string TrainingDataFolder { get; set; }
            public string ContainerName { get; set; }
            public string StorageAccountName { get; set; }
            public List<SpeechSubscription> SpeechKeys { get; set; }
        }

        public class SpeechSubscription
        {
            public string SubscriptionKey { get; set; }
            public string Name { get; set; }
            public string Region { get; set; }
        }
    }
}
